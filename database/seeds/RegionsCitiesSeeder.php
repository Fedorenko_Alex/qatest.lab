<?php


use Phinx\Seed\AbstractSeed;

class RegionsCitiesSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = json_decode('[{"name":"АВТОНОМНА РЕСПУБЛІКА КРИМ","cities":["СІМФЕРОПОЛЬ","АЛУШТА","ДЖАНКОЙ","ЄВПАТОРІЯ","КЕРЧ","КРАСНОПЕРЕКОПСЬК","САКИ","АРМЯНСЬК","ФЕОДОСІЯ","СУДАК","ЯЛТА"]},{"name":"ВІННИЦЬКА ОБЛАСТЬ","cities":["ВІННИЦЯ","ЖМЕРИНКА","МОГИЛІВ-ПОДІЛЬСЬКИЙ","КОЗЯТИН","ЛАДИЖИН","ХМІЛЬНИК"]},{"name":"ВОЛИНСЬКА ОБЛАСТЬ","cities":["ЛУЦЬК","ВОЛОДИМИР-ВОЛИНСЬКИЙ","КОВЕЛЬ","НОВОВОЛИНСЬК"]},{"name":"ДНІПРОПЕТРОВСЬКА ОБЛАСТЬ","cities":["ДНІПРО","ВІЛЬНОГІРСЬК","КАМ\'ЯНСЬКЕ","ЖОВТІ ВОДИ","КРИВИЙ РІГ","МАРГАНЕЦЬ","НІКОПОЛЬ","НОВОМОСКОВСЬК","ПОКРОВ","ПАВЛОГРАД","ПЕРШОТРАВЕНСЬК","СИНЕЛЬНИКОВЕ","ТЕРНІВКА"]},{"name":"ДОНЕЦЬКА ОБЛАСТЬ","cities":["ДОНЕЦЬК","АВДІЇВКА","БАХМУТ","ГОРЛІВКА","ДЕБАЛЬЦЕВЕ","ТОРЕЦЬК","МИРНОГРАД","ДОБРОПІЛЛЯ","ДОКУЧАЄВСЬК","ДРУЖКІВКА","ЄНАКІЄВЕ","ЖДАНІВКА","МАРІУПОЛЬ","ХРЕСТІВКА","КОСТЯНТИНІВКА","КРАМАТОРСЬК","ПОКРОВСЬК","ЛИМАН","МАКІЇВКА","НОВОГРОДІВКА","СЕЛИДОВЕ","СЛОВ\'ЯНСЬК","СНІЖНЕ","ЧИСТЯКОВЕ","ВУГЛЕДАР","ХАРЦИЗЬК","ШАХТАРСЬК","ЯСИНУВАТА"]},{"name":"ЖИТОМИРСЬКА ОБЛАСТЬ","cities":["ЖИТОМИР","БЕРДИЧІВ","КОРОСТЕНЬ","МАЛИН","НОВОГРАД-ВОЛИНСЬКИЙ"]},{"name":"ЗАКАРПАТСЬКА ОБЛАСТЬ","cities":["УЖГОРОД","БЕРЕГОВЕ","МУКАЧЕВО","ХУСТ","ЧОП"]},{"name":"ЗАПОРІЗЬКА ОБЛАСТЬ","cities":["ЗАПОРІЖЖЯ","БЕРДЯНСЬК","МЕЛІТОПОЛЬ","ТОКМАК","ЕНЕРГОДАР"]},{"name":"ІВАНО-ФРАНКІВСЬКА ОБЛАСТЬ","cities":["ІВАНО-ФРАНКІВСЬК","БОЛЕХІВ","БУРШТИН","КАЛУШ","КОЛОМИЯ","ЯРЕМЧЕ"]},{"name":"КИЇВСЬКА ОБЛАСТЬ","cities":["БІЛА ЦЕРКВА","БЕРЕЗАНЬ","БОРИСПІЛЬ","БРОВАРИ","ВАСИЛЬКІВ","БУЧА","ІРПІНЬ","ПЕРЕЯСЛАВ-ХМЕЛЬНИЦЬКИЙ","ПРИП\'ЯТЬ","ФАСТІВ","РЖИЩІВ","СЛАВУТИЧ","ОБУХІВ"]},{"name":"КІРОВОГРАДСЬКА ОБЛАСТЬ","cities":["КРОПИВНИЦЬКИЙ","ОЛЕКСАНДРІЯ","ЗНАМ\'ЯНКА","СВІТЛОВОДСЬК"]},{"name":"ЛУГАНСЬКА ОБЛАСТЬ","cities":["ЛУГАНСЬК","АНТРАЦИТ","БРЯНКА","ГОЛУБІВКА","АЛЧЕВСЬК","СОРОКИНЕ","ХРУСТАЛЬНИЙ","ЛИСИЧАНСЬК","ПЕРВОМАЙСЬК","РОВЕНЬКИ","РУБІЖНЕ","ДОВЖАНСЬК","СЄВЄРОДОНЕЦЬК","КАДІЇВКА"]},{"name":"ЛЬВІВСЬКА ОБЛАСТЬ","cities":["ЛЬВІВ","БОРИСЛАВ","ДРОГОБИЧ","МОРШИН","НОВИЙ РОЗДІЛ","САМБІР","СТРИЙ","ТРУСКАВЕЦЬ","ЧЕРВОНОГРАД"]},{"name":"МИКОЛАЇВСЬКА ОБЛАСТЬ","cities":["МИКОЛАЇВ","ВОЗНЕСЕНСЬК","ОЧАКІВ","ПЕРВОМАЙСЬК","ЮЖНОУКРАЇНСЬК"]},{"name":"ОДЕСЬКА ОБЛАСТЬ","cities":["ОДЕСА","БАЛТА","БІЛГОРОД-ДНІСТРОВСЬКИЙ","БІЛЯЇВКА","ІЗМАЇЛ","ЧОРНОМОРСЬК","ПОДІЛЬСЬК","ТЕПЛОДАР","ЮЖНЕ"]},{"name":"ПОЛТАВСЬКА ОБЛАСТЬ","cities":["ПОЛТАВА","ГОРІШНІ ПЛАВНІ","ГАДЯЧ","КРЕМЕНЧУК","ЛУБНИ","МИРГОРОД"]},{"name":"РІВНЕНСЬКА ОБЛАСТЬ","cities":["РІВНЕ","ДУБНО","ВАРАШ","ОСТРОГ"]},{"name":"СУМСЬКА ОБЛАСТЬ","cities":["СУМИ","ОХТИРКА","ГЛУХІВ","КОНОТОП","ЛЕБЕДИН","РОМНИ","ШОСТКА"]},{"name":"ТЕРНОПІЛЬСЬКА ОБЛАСТЬ","cities":["ТЕРНОПІЛЬ","ЧОРТКІВ","БЕРЕЖАНИ","КРЕМЕНЕЦЬ"]},{"name":"ХАРКІВСЬКА ОБЛАСТЬ","cities":["ХАРКІВ","ІЗЮМ","КУП\'ЯНСЬК","ЛОЗОВА","ЛЮБОТИН","ПЕРВОМАЙСЬКИЙ","ЧУГУЇВ"]},{"name":"ХЕРСОНСЬКА ОБЛАСТЬ","cities":["ХЕРСОН","ГОЛА ПРИСТАНЬ","КАХОВКА","НОВА КАХОВКА"]},{"name":"ХМЕЛЬНИЦЬКА ОБЛАСТЬ","cities":["ХМЕЛЬНИЦЬКИЙ","КАМ\'ЯНЕЦЬ-ПОДІЛЬСЬКИЙ","НЕТІШИН","СЛАВУТА","ШЕПЕТІВКА","СТАРОКОСТЯНТИНІВ"]},{"name":"ЧЕРКАСЬКА ОБЛАСТЬ","cities":["ЧЕРКАСИ","ВАТУТІНЕ","КАНІВ","ЗОЛОТОНОША","СМІЛА","УМАНЬ"]},{"name":"ЧЕРНІВЕЦЬКА ОБЛАСТЬ","cities":["ЧЕРНІВЦІ","НОВОДНІСТРОВСЬК"]},{"name":"ЧЕРНІГІВСЬКА ОБЛАСТЬ","cities":["ЧЕРНІГІВ","НІЖИН","НОВГОРОД-СІВЕРСЬКИЙ","ПРИЛУКИ"]}]', true);
        foreach ($data as $region) {
            $this->table('dictionary_regions')->insert([
                'name' => $region['name'],
            ])->save();
            $region_id = $this->getAdapter()->getConnection()->lastInsertId();
            foreach ($region['cities'] as $value) {

                $this->table('dictionary_cities')->insert([
                    'name' => $value,
                    'region_id' => $region_id,
                ])->save();

            }
        }
    }
}
