<?php

use Phinx\Migration\AbstractMigration;

class CreateDictionariesForeignKeys extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    public function up()
    {
        $cities = $this->table('dictionary_cities');
        $cities
            ->addForeignKey('region_id', 'dictionary_regions', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->save();

        $models = $this->table('dictionary_models');
        $models
            ->addForeignKey('mark_id', 'dictionary_marks', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->save();
    }

    public function down()
    {
        $cities = $this->table('dictionary_cities');
        $cities
            ->dropForeignKey('region_id')
            ->save();

        $models = $this->table('dictionary_models');
        $models
            ->dropForeignKey('mark_id')
            ->save();
    }
}
