<?php

use Phinx\Migration\AbstractMigration;

class CreateAdvertsForeignKeys extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    public function up()
    {
        $adverts = $this->table('adverts');
        $adverts
            ->addForeignKey('user_id', 'users', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->addForeignKey('model_id', 'dictionary_models', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->addForeignKey('city_id', 'dictionary_cities', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->save();

        $photos = $this->table('photos');
        $photos
            ->addForeignKey('advert_id', 'adverts', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->save();
    }

    public function down()
    {
        $adverts = $this->table('adverts');
        $adverts
            ->dropForeignKey('user_id')
            ->dropForeignKey('model_id')
            ->dropForeignKey('city_id')
            ->save();

        $photos = $this->table('photos');
        $photos
            ->dropForeignKey('advert_id')
            ->save();
    }
}
