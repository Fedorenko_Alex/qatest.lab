<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class CreateAdverts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    public function up()
    {
        $table = $this->table('adverts', ['signed' => false]);
        $table->addColumn('user_id', 'integer', ['signed' => false])
            ->addColumn('model_id', 'integer', ['signed' => false])
            ->addColumn('city_id', 'integer', ['signed' => false])
            ->addColumn('engine_capacity', 'decimal', ['precision' => 10, 'scale' => 2])
            ->addColumn('mileage', 'integer', ['signed' => false])
            ->addColumn('used', 'integer', ['limit' => MysqlAdapter::INT_TINY, 'default' => 1])
            ->addColumn('status', 'integer', ['limit' => MysqlAdapter::INT_TINY, 'default' => 1])
            ->addTimestamps()
            ->addIndex('user_id')
            ->addIndex(['model_id', 'city_id'])
            ->addIndex(['model_id', 'city_id', 'mileage'])
            ->addIndex('engine_capacity')
            ->addIndex('used')
            ->create();
    }

    public function down()
    {
        $this->table('adverts')->drop()->save();
    }
}
