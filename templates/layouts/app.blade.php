<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Автопродажник</title>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="../../assets/js/custom.js"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../../assets/vendor/fa/css/font-awesome.min.css">
    <link href="../../assets/css/custom.css" rel="stylesheet">
</head>
<body>
<div class="app">
    <div class="wrap">
        <nav class="navbar navbar-expand-md navbar-light">
            <div class="container">
                <a href="http://{{_SITE_NAME_}}/" class="navbar-brand d-flex align-items-center text-dark">
                    Автопродаж
                </a>

                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        @if($is_auth)
                            <li class="nav-item">
                                <a class="nav-link  text-dark" tabindex="-1" aria-disabled="true">{{ $login }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link  text-dark ml-4" tabindex="-1" aria-disabled="true"
                                   href="http://{{_SITE_NAME_}}/auth/signOut">Вихід</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link text-dark" tabindex="-1" aria-disabled="true" href="http://{{_SITE_NAME_}}/auth/signIn">Вхід</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-dark" tabindex="-1" aria-disabled="true" href="http://{{_SITE_NAME_}}/auth/register">Реєстрація</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('page')

    </div>

    <script>
        current_page = 1;
    </script>

    <div class="modal fade" id="universalModal" tabindex="-1" role="dialog" aria-labelledby="universalModal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

            </div>
        </div>
    </div>

    <footer class="page-footer font-small border-top">

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">© 2019 Copyright:
            <a href="http://{{_SITE_NAME_}}/" class="text-dark"> Автопродаж</a>
        </div>
        <!-- Copyright -->

    </footer>
</div>
</body>
</html>
