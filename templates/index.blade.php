@extends('layouts.app')

@section('page')
    <section class="pt-5 pb-5 ">
        <div class="container">
            <div class="row justify-content-left">
                <div class="col-md-8">
                    <div class="card">
                        <h5 class="card-header ">Пошук авто</h5>
                        <div class="card-body avenir-thin">

                            <form method="POST" action="http://{{_SITE_NAME_}}/posters/search">

                                <div class="form-group form-inline justify-content-start">
                                    <div class="custom-control custom-radio mr-3">
                                        <input type="radio" id="used1" name="used" value="0"
                                               class="custom-control-input">
                                        <label class="custom-control-label" for="used1">Нове авто</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="used2" name="used" value="1"
                                               class="custom-control-input">
                                        <label class="custom-control-label" for="used2">Вживане авто</label>
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <select class="form-control" name="region_id" id="regions">
                                                <option value="0">Всі області</option>
                                                @foreach ($regions as $id => $region)
                                                    <option value="{{$id}}">{{$region}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <select class="form-control" name="mark_id" id="marks">
                                                <option value="0">Всі марки</option>
                                                @foreach ($marks as $id => $mark)
                                                    <option value="{{$id}}">{{$mark}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <select class="form-control @isset($city_id_error) is-invalid @endisset"
                                                    name="city_id" id="cities">
                                                <option value="0">Місто</option>
                                                @foreach ($cities as $id => $city)
                                                    <option value="{{$id}}"
                                                            data-region="{{$city['region_id']}}">{{$city['name']}}</option>
                                                @endforeach
                                            </select>
                                            @isset($city_id_error)
                                                <small class="text-danger">{{$city_id_error}}</small>
                                            @endisset
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <select class="form-control @isset($model_id_error) is-invalid @endisset"
                                                    name="model_id" id="models">
                                                <option value="0">Модель</option>
                                                @foreach ($models as $id => $model)
                                                    <option value="{{$id}}"
                                                            data-mark="{{$model['mark_id']}}">{{$model['name']}}</option>
                                                @endforeach
                                            </select>
                                            @isset($model_id_error)
                                                <small class="text-danger">{{$model_id_error}}</small>
                                            @endisset
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group form-inline justify-content-end">
                                            <label for="engine_capacity_from">Об’єм двигуна (л.) </label>
                                            <input type="number"
                                                   class="form-control ml-2 size6  @isset($engine_capacity_from_error) is-invalid @endisset"
                                                   placeholder="від"
                                                   size="6" name="engine_capacity_from" id="engine_capacity_from">
                                            @isset($engine_capacity_from_error)
                                                <small class="text-danger">{{$engine_capacity_from_error}}</small>
                                            @endisset
                                            <input type="number"
                                                   class="form-control ml-2 size6 @isset($engine_capacity_to_error) is-invalid @endisset"
                                                   placeholder="до"
                                                   size="6" name="engine_capacity_to" id="engine_capacity_to">
                                            @isset($engine_capacity_to_error)
                                                <small class="text-danger">{{$engine_capacity_to_error}}</small>
                                            @endisset
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group form-inline justify-content-end  ">
                                            <label for="mileage_from">Пробіг (тис. км) </label>
                                            <input type="number"
                                                   class="form-control ml-2 size6 @isset($mileage_from_error) is-invalid @endisset"
                                                   placeholder="від"
                                                   size="6" name="mileage_from" id="mileage_from">
                                            @isset($mileage_from_error)
                                                <small class="text-danger">{{$mileage_from_error}}</small>
                                            @endisset
                                            <input type="number"
                                                   class="form-control ml-2 size6 @isset($mileage_to_error) is-invalid @endisset"
                                                   placeholder="до"
                                                   size="6" name="mileage_to" id="mileage_to">
                                            @isset($mileage_to_error)
                                                <small class="text-danger">{{$mileage_to_error}}</small>
                                            @endisset
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-inline justify-content-end">
                                    <button type="submit" class="btn btn-primary">Пошук</button>
                                </div>

                            </form>

                            <script>
                                @if($city_id)
                                setCity({{$city_id}});
                                @elseif($region_id)
                                setRegion({{$region_id}});
                                @endif
                                @if($model_id)
                                setModel({{$model_id}});
                                @elseif($mark_id)
                                setMark({{$mark_id}});
                                @endif
                            </script>
                        </div>

                    </div>
                </div>

                <div class="col-md-4">
                    @if($is_auth)
                        @component('components.my_adverts',['add_icon' => $add_icon, 'user_adv'=>$user_adv])
                        @endcomponent
                    @endif
                </div>
            </div>

        </div>
    </section>

    <div class="container mb-5">
        <div class="row justify-content-md-center">

            <div class="col">
                <h2>Нові оголошення</h2>

                @if(is_array($last_adverts))
                    <div id="carouselLastIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach($last_adverts as $key=> $advert)
                                <li data-target="#carouselLastIndicators" data-slide-to="{{$key}}"
                                    @if($key == 0)
                                    class="active"
                                        @endif></li>
                            @endforeach
                        </ol>
                        <div class="carousel-inner">
                            @foreach($last_adverts as $key=> $advert)
                                <div class="carousel-item carousel-image
                                @if($key == 0)
                                active
                                @endif"
                                style="background-image: url('../uploads/{{$advert['file_name']}}');" data-advert="{{$advert['id']}}">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>{{$advert['mark']}} {{$advert['model']}}</h5>
                                        <p>{{($advert['used']?'Вживана':'Нова')}} Об'єм двигуна (л.): {{$advert['engine_capacity']}} Пробіг (тис. км): {{$advert['mileage']}} {{$advert['region']}} {{$advert['city']}}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselLastIndicators" role="button"
                           data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselLastIndicators" role="button"
                           data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                @endif

            </div>

            <div class="col">
                <h2>Нещодавні перегляди</h2>
                @if(is_array($seen_adverts))
                    <div id="carouselSeenIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach($seen_adverts as $key=> $advert)
                                <li data-target="#carouselSeenIndicators" data-slide-to="{{$key}}"
                                    @if($key == 0)
                                    class="active"
                                        @endif></li>
                            @endforeach
                        </ol>
                        <div class="carousel-inner">
                            @foreach($seen_adverts as $key=> $advert)
                                <div class="carousel-item carousel-image
                                @if($key == 0)
                                        active
                                @endif"
                                     style="background-image: url('../uploads/{{$advert['file_name']}}');" data-advert="{{$advert['id']}}">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>{{$advert['mark']}} {{$advert['model']}}</h5>
                                        <p>{{($advert['used']?'Вживана':'Нова')}} Об'єм двигуна (л.): {{$advert['engine_capacity']}} Пробіг (тис. км): {{$advert['mileage']}} {{$advert['region']}} {{$advert['city']}}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselSeenIndicators" role="button"
                           data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselSeenIndicators" role="button"
                           data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                @endif
            </div>

        </div>
    </div>

@endsection