@extends('layouts.app')

@section('page')

    <div class="container mb-5">

        <div class="row justify-content-md-begin mt-4">
            <div class="col">
                <h4 class="avenir-demi">Сайт з продажу авто №1</h4>
                <span>За Вашим запитом пропозицій знайдено - {{$count}}</span>
            </div>
        </div>

        <div class="row bg-white mt-4">

            <div class="col-3 pt-4">

                <form method="POST" action="http://{{_SITE_NAME_}}/posters/search">

                    <div class="form-group">
                        <select class="form-control" name="region_id" id="regions">
                            <option value="0">Всі області</option>
                            @foreach ($regions as $id => $region)
                                <option value="{{$id}}">{{$region}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control @isset($city_id_error) is-invalid @endisset"
                                name="city_id" id="cities">
                            <option value="0">Місто</option>
                            @foreach ($cities as $id => $city)
                                <option value="{{$id}}"
                                        data-region="{{$city['region_id']}}">{{$city['name']}}</option>
                            @endforeach
                        </select>
                        @isset($city_id_error)
                            <small class="text-danger">{{$city_id_error}}</small>
                        @endisset
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="mark_id" id="marks">
                            <option value="0">Всі марки</option>
                            @foreach ($marks as $id => $mark)
                                <option value="{{$id}}">{{$mark}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control @isset($model_id_error) is-invalid @endisset"
                                name="model_id" id="models">
                            <option value="0">Модель</option>
                            @foreach ($models as $id => $model)
                                <option value="{{$id}}"
                                        data-mark="{{$model['mark_id']}}">{{$model['name']}}</option>
                            @endforeach
                        </select>
                        @isset($model_id_error)
                            <small class="text-danger">{{$model_id_error}}</small>
                        @endisset
                    </div>
                    <div class="form-group justify-content-begin">
                        <label for="engine_capacity_from">Об’єм двигуна (л.) </label>
                        <div class="row">
                            <div class="col">
                                <input type="number"
                                       class="form-control ml-2 size6  @isset($engine_capacity_from_error) is-invalid @endisset"
                                       placeholder="від"
                                       value="@isset($engine_capacity_from){{$engine_capacity_from}}@endisset" size="6"
                                       name="engine_capacity_from" id="engine_capacity_from">
                                @isset($engine_capacity_from_error)
                                    <small class="text-danger">{{$engine_capacity_from_error}}</small>
                                @endisset
                            </div>
                            <div class="col">
                                <input type="number"
                                       class="form-control ml-2 size6 @isset($engine_capacity_to_error) is-invalid @endisset"
                                       placeholder="до"
                                       value="@isset($engine_capacity_to){{$engine_capacity_to}}@endisset" size="6"
                                       name="engine_capacity_to" id="engine_capacity_to">
                                @isset($engine_capacity_to_error)
                                    <small class="text-danger">{{$engine_capacity_to_error}}</small>
                                @endisset
                            </div>


                        </div>
                    </div>
                    <div class="form-group justify-content-begin  ">
                        <label for="mileage_from">Пробіг (тис. км) </label>
                        <div class="row">
                            <div class="col">
                                <input type="number"
                                       class="form-control ml-2 size6 @isset($mileage_from_error) is-invalid @endisset"
                                       placeholder="від"
                                       value="@isset($mileage_from){{$mileage_from}}@endisset" size="6"
                                       name="mileage_from" id="mileage_from">
                                @isset($mileage_from_error)
                                    <small class="text-danger">{{$mileage_from_error}}</small>
                                @endisset
                            </div>
                            <div class="col">
                                <input type="number"
                                       class="form-control ml-2 size6 @isset($mileage_to_error) is-invalid @endisset"
                                       placeholder="до"
                                       value="@isset($mileage_to){{$mileage_to}}@endisset" size="6" name="mileage_to"
                                       id="mileage_to">
                                @isset($mileage_to_error)
                                    <small class="text-danger">{{$mileage_to_error}}</small>
                                @endisset
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-inline justify-content-between">
                        <div class="custom-control custom-radio mr-3">
                            <input type="radio" id="used1" name="used" value="0"
                                   class="custom-control-input" {{(isset($used)&&$used==0)?'checked':''}}>
                            <label class="custom-control-label" for="used1">Нове авто</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="used2" name="used" value="1"
                                   class="custom-control-input" {{(isset($used)&&$used==1)?'checked':''}}>
                            <label class="custom-control-label" for="used2">Вживане авто</label>
                        </div>

                    </div>

                    <div class="form-group form-inline justify-content-end">
                        <button type="submit" class="btn btn-primary">Пошук</button>
                    </div>
                    <script>
                        @if($city_id)
                        setCity({{$city_id}});
                        @elseif($region_id)
                        setRegion({{$region_id}});
                        @endif
                        @if($model_id)
                        setModel({{$model_id}});
                        @elseif($mark_id)
                        setMark({{$mark_id}});
                        @endif
                    </script>
                </form>

            </div>

            <div class="col p-0 pt-4">
                <div class="list-group list-group-flush">
                    @if(is_array($adverts))
                        @foreach($adverts as $advert)
                            <a href="#" class="list-group-item list-group-item-action d-flex flex-row"
                               data-advert="{{$advert['id']}}">
                                <img src="../uploads/{{$advert['file_name']}}" class="align-self-center mr-3 pull-left"
                                     width="128" alt="...">
                                <div class="d-flex flex-column w-100">
                                    <div class="">
                                        <h5 class="mb-1 pull-left">{{$advert['mark']}} {{$advert['model']}}</h5>
                                        <small class="pull-right">{{$advert['created_at']}}</small>
                                    </div>
                                    <p class="mb-1">{{($advert['used']?'Вживана':'Нова')}}</p>
                                    <p class="mb-1">Об'єм двигуна (л.): {{$advert['engine_capacity']}}</p>
                                    <p class="mb-1">Пробіг (тис. км): {{$advert['mileage']}}</p>
                                    <small>{{$advert['region']}} {{$advert['city']}}</small>
                                </div>
                            </a>
                        @endforeach
                    @else
                        <h5>За вашим запитом нічого не знайдено</h5>
                    @endif
                </div>
                <nav aria-label="" class="d-flex justify-content-center mt-4">
                    @if($pages_count>1)
                        <ul class="pagination pagination-sm" id="paginator">
                            <li class="page-item" id="nextPage"><a class="page-link" href="#" data-page="next"> Наступна >> </a></li>
                        </ul>
                    @endif
                </nav>
            </div>

        </div>

    </div>

@endsection