<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        <h5>Мої оголошення</h5>
        @if($add_icon)
            <a href="http://{{_SITE_NAME_}}/posters/create" class="card-link pull-right"><i class="fa fa-plus"></i></a>
        @endif
    </div>
    <div class="card-body avenir-thin p-0">
        <div class="list-group">
            @if(is_array($user_adv))
                @foreach ($user_adv as $adv)
                    <div class="list-group-item p-1 list-group-item-action flex-column align-items-starts">
                        <div class="d-flex w-100 justify-content-between">
                            <span  data-advert="{{$adv['id']}}">
                            <h5 class="mb-1">{{$adv['mark']}} {{$adv['model']}}</h5>
                            <small>{{$adv['created_at']}}</small>
                            </span>
                            <a href="http://{{_SITE_NAME_}}/posters/delete/{{$adv['id']}}" class="card-link pull-right"><i
                                        class="fa fa-times text-danger"></i></a>
                        </div>
                        <p class="mb-1">{{($adv['used']?'Вживана':'Нова')}}; Об'єм двигуна
                            (л.): {{$adv['engine_capacity']}}; Пробіг (тис. км): {{$adv['mileage']}}</p>
                        <small>{{$adv['region']}} {{$adv['city']}}</small>
                    </div>
                @endforeach
            @endif
        </div>

    </div>

</div>