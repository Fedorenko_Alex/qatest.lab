@extends('layouts.app')

@section('page')
    <section class="mt-3 bg-transparent ">
        <div class="container">

            <div class="row">
                <div class="col">
                    <button data-url="http://{{_SITE_NAME_}}/" class="btn btn-primary mb-3" type="button"><i
                                class="fa fa-chevron-left"></i> На головну
                    </button>
                </div>
            </div>

            <div class="row justify-content-left">
                <div class="col-md-8">
                    <div class="card">
                        <h5 class="card-header ">Створити оголошення</h5>
                        <div class="card-body avenir-thin">

                            <form method="POST" action="http://{{_SITE_NAME_}}/posters/createProcess"
                                  enctype='multipart/form-data' aria-label="Увійти">

                                <div class="form-group form-inline justify-content-start">
                                    <div class="custom-control custom-radio mr-3">
                                        <input type="radio" id="used1" name="used" value="0"
                                               class="custom-control-input" required
                                        @isset($used)
                                        {{($used == 0)?'checked':''}}
                                        @endisset
                                        >
                                        <label class="custom-control-label" for="used1">Нове авто</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="used2" name="used" value="1"
                                               class="custom-control-input" required
                                        @isset($used)
                                            {{($used == 1)?'checked':''}}
                                        @endisset
                                        >
                                        <label class="custom-control-label" for="used2">Вживане авто</label>
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <select class="form-control" name="region_id" id="regions">
                                                <option value="0">Всі області</option>
                                                @foreach ($regions as $id => $region)
                                                    <option value="{{$id}}">{{$region}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <select class="form-control" name="mark_id" id="marks">
                                                <option value="0">Всі марки</option>
                                                @foreach ($marks as $id => $mark)
                                                    <option value="{{$id}}">{{$mark}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <select class="form-control @isset($city_id_error) is-invalid @endisset"
                                                    name="city_id" id="cities">
                                                <option value="0">Місто</option>
                                                @foreach ($cities as $id => $city)
                                                    <option value="{{$id}}"
                                                            data-region="{{$city['region_id']}}">{{$city['name']}}</option>
                                                @endforeach
                                            </select>
                                            @isset($city_id_error)
                                                <small class="text-danger">{{$city_id_error}}</small>
                                            @endisset
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <select class="form-control @isset($model_id_error) is-invalid @endisset"
                                                    name="model_id" id="models">
                                                <option value="0">Модель</option>
                                                @foreach ($models as $id => $model)
                                                    <option value="{{$id}}"
                                                            data-mark="{{$model['mark_id']}}">{{$model['name']}}</option>
                                                @endforeach
                                            </select>
                                            @isset($model_id_error)
                                                <small class="text-danger">{{$model_id_error}}</small>
                                            @endisset
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group form-inline justify-content-end">
                                            <label for="engine_capacity">Об’єм двигуна </label>
                                            <input type="number" step="0.1"
                                                   class="form-control ml-2 @isset($engine_capacity_error) is-invalid @endisset"
                                                   value="@isset($engine_capacity){{$engine_capacity}}@endisset"
                                                   placeholder="(л.)" id="engine_capacity" name="engine_capacity"
                                                   required>
                                            @isset($engine_capacity_error)
                                                <small class="text-danger">{{$engine_capacity_error}}</small>
                                            @endisset
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group form-inline justify-content-end  ">
                                            <label for="mileage">Пробіг </label>
                                            <input type="number"
                                                   class="form-control ml-2 @isset($mileage_error) is-invalid @endisset"
                                                   value="@isset($mileage){{$mileage}}@endisset"
                                                   placeholder="(тис. км)" id="mileage" name="mileage" required>
                                            @isset($mileage_error)
                                                <small class="text-danger">{{$mileage_error}}</small>
                                            @endisset
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="custom-file">

                                            <input
                                                    type="file"
                                                    class="custom-file-input @isset($photos_error) is-invalid @endisset"
                                                    id="photos"
                                                    name="photos[]"
                                                    multiple="true"
                                                    required>

                                            <label class="custom-file-label overflow-hidden"
                                                   for="photos"
                                                   data-browse="Завантажити фото"></label>
                                            @isset($photos_error)
                                                <small class="text-danger">{{$photos_error}}</small>
                                            @endisset
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-inline justify-content-end mt-2">
                                    <button type="submit" class="btn btn-primary">Створити оголошення</button>
                                </div>

                            </form>
                        </div>

                        <script>
                            @if($region_id)
                            setRegion({{$region_id}});
                            @elseif($city_id)
                            setCity({{$city_id}});
                            @endif
                            @if($mark)
                            setMark({{$mark_id}});
                            @elseif($model_id)
                            setModel({{$model_id}});
                            @endif
                        </script>
                    </div>
                </div>

                <div class="col-md-4">
                    @component('components.my_adverts', ['add_icon' => false, 'user_adv'=>$user_adv])
                    @endcomponent
                </div>
            </div>

        </div>
    </section>
@endsection