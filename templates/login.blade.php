@extends('layouts.app')

@section('page')
    <div class="row justify-content-center pt-5 ">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Авторизація</div>
                <div class="card-body">
                    <form method="POST" action="http://{{_SITE_NAME_}}/auth/signInProcess" aria-label="Увійти">
                        <input type="hidden" name="_token" value="wp3bCtQTrlKkPuEJQWvQPYs3JYMSHVrZlC4rd1B8">
                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">Електронна пошта</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control  @isset($email_error) is-invalid @endisset" name="email" value="@isset($email) {{$email}} @endisset" required="">
                                @isset($email_error)
                                    <small class="text-danger">{{$email_error}}</small>
                                @endisset
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Пароль</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @isset($password_error) is-invalid @endisset" name="password" required="">
                                @isset($password_error)
                                    <small class="text-danger">{{$password_error}}</small>
                                @endisset
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Увійти
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection