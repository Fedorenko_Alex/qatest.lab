<?php


namespace Kernel;


use DataProviders\MysqlDB;

class Model
{

    /**
     * @var $DB MysqlDB - класс провайдера данных, с которым происходит работа в моделях
     */
    protected $DB;

    public function __construct()
    {
        $this->DB = new MysqlDB();
    }
}