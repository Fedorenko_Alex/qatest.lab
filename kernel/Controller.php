<?php


namespace Kernel;


use Models\Adverts;
use Models\Cities;
use Models\Models;
use Models\Users;
use Responses\HttpResponse;
use Services\Filters;
use Services\Validator;
use Exception;

class Controller
{
    private $response;
    private $model_container;
    private $service_container;

    public function __construct(\iResponse $response = null)
    {
        if (is_null($response)) {
            $this->response = new HttpResponse();
        } else {
            $this->response = $response;
        }

        $this->modelsBootstrap();
        $this->servicesBootstrap();
    }

    /**
     * Вызывает метод отправки json ответа из подключенного класа Response
     *
     * @param $json - json строка ответа
     * @return mixed
     */
    protected function sendJson($json)
    {
        return ($this->response)->json($json);
    }

    /**
     * Вызывает метод отправки сооббщения об ошибке из подключенного класа Response
     *
     * @param int $err_code - номер ошибки
     * @return mixed
     */
    protected function sendError(int $err_code = 404)
    {
        return ($this->response)->error($err_code);
    }

    /**
     * Вызывает метод отправки шаблона из подключенного класа Response
     *
     * @param string $template_name - название blade шаблона
     * @param array $params - параметры, которые будут переданы в шаблон
     * @return mixed
     * @throws Exception
     */
    protected function sendTemplate(string $template_name, array $params = [])
    {
        if (($params['is_auth'] = $this->model('users')->isAuth()) !== false) {
            $params['login'] = $this->model('users')->getEmail($params['is_auth']);
        }

        return ($this->response)->template($template_name, $params);
    }

    /**
     * Вызывает метод перенаправления на страницу из подключенного класа Response
     *
     * @param $url - адрес, на которой произойдет перенаправление
     * @return mixed
     */
    protected function redirect($url)
    {
        return ($this->response)->redirect($url);
    }

    /**
     * Создание экземпляров классов моделей и их упаковка в контейнер моделей.
     */
    private function modelsBootstrap()
    {
        $this->model_container['adverts'] = new Adverts();
        $this->model_container['cities'] = new Cities();
        $this->model_container['models'] = new Models();
        $this->model_container['users'] = new Users();
    }

    /**
     * Создание экземпляров классов сервисов и их упаковка в контейнер моделей.
     */
    private function servicesBootstrap()
    {
        $this->service_container['filters'] = new Filters($this->model_container);
        $this->service_container['validator'] = new Validator($this->model_container);
    }

    /**
     * Вызов модели посредствам контейнера моделей.
     *
     * @param string $model_name - название модели
     * @return mixed
     * @throws Exception
     */
    protected function model(string $model_name)
    {
        if (isset($this->model_container[$model_name]))
            return $this->model_container[$model_name];
        else {
            throw new Exception('Undefined model ' . $model_name);
        }

    }

    /**
     * Вызов сервиса посредствам контейнера сервисов.
     *
     * @param string $service_name - название сервиса
     * @return mixed
     * @throws Exception
     */
    protected function service(string $service_name)
    {
        if (isset($this->service_container[$service_name]))
            return $this->service_container[$service_name];
        else {
            throw new Exception('Undefined service ' . $service_name);
        }

    }
}