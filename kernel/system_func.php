<?php

/**
 * Запись факта возникновения ошибки в log файл.
 *
 * @param $type - тип возникнувшей ошибки
 * @param $class - класс, в котором произошла ошибка
 * @param $method - метод, в котором произошла ошибка
 * @param $message_income - описание ошибки
 */
function __error_log($type, $class, $method, $message_income)
{
    $file = _ROOT_ . '/log/' . date('dmY') . '.txt';
    $written_message = json_encode([
            'time' => date('d.m.Y H:i:s') . ' ' . time(),
            'error_type' => $type,
            'location' => $class . '::' . $method,
            'description' => $message_income
        ]) . PHP_EOL;
    file_put_contents($file, $written_message, FILE_APPEND);

}