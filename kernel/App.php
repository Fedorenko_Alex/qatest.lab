<?php


namespace Kernel;

use Exception;
use Responses\HttpResponse;

class App
{

    private $controller;
    private $method;
    private $param = false;

    public function __construct()
    {
        if ($this->defineParams()) {

            $this->createSession();

            try {
                if (class_exists($this->controller)) {
                    $controller = new $this->controller();
                    $method_name = $this->method;
                    if (method_exists($controller, $method_name)) {
                        if ($this->param) {
                            $controller->$method_name($this->param);
                        } else {
                            $controller->$method_name();
                        }
                        return true;
                    }
                }

            } catch (Exception $e) {
                __error_log('unvalid_uri', __CLASS__, __METHOD__, $e->getMessage());
                if ($GLOBALS['configuration']['display_error']) {
                    var_dump($e->getMessage());
                }

                return (new HttpResponse())->error(500);


            }

        }
        return (new HttpResponse())->error(404);
    }

    /**
     * Вызов метода соответствующего контроллера на основе URL строки
     *
     * @return bool
     */
    private function defineParams()
    {
        if (($request_uri = $this->checkRequestUri()) !== false) {

            $request_uri = explode('?', $request_uri);

            $request_uri = $request_uri[0];
            $count_request_ = strlen($request_uri);
            if ($count_request_ > 0 && $request_uri != '') {
                // Разбиваем на элементы
                $request_uri = explode('/', $request_uri);

                if (isset($request_uri[0])) {
                    $this->controller = 'Controllers\\' . $request_uri[0];
                    if (isset($request_uri[1])) {
                        $this->method = $request_uri[1];
                        if (isset($request_uri[2])) {
                            $this->param = $request_uri[2];
                        }
                        return true;
                    }
                }
            }

            $this->controller = 'Controllers\Welcome';
            $this->method = 'index';
            return true;
        } else {
            return false;
        }

    }

    /**
     * Проверка строки запроса на отсутствие недопустимых символов
     *
     * @return bool|string
     */
    private function checkRequestUri()
    {
        $request_uri = substr(trim($_SERVER['REQUEST_URI']), 1);
        if (strlen($request_uri) > 255)
            return false;
        if (preg_match("/[^A-Za-z0-9\?\&\_\/\-\=\%]/", $request_uri))
            return false;
        return $request_uri;
    }

    /**
     * Старт сессии
     */
    private function createSession()
    {
        session_start();
    }
}