<?php


namespace Controllers;

use Kernel\Controller;

class Posters extends Controller
{
    /**
     * Рендер страницы создания объявления
     * returns template
     *
     * @throws \Exception
     */
    public function create()
    {
        if (($user_id = $this->model('users')->isAuth()) && ($this->model('adverts')->creatingAllowed($user_id))) {
            $this->sendTemplate('create_advert', $this->buildFilterDictionaries());
        } else {
            $this->redirect(_SITE_NAME_ . '/');
        }

    }

    /**
     * Обработка данных создания объявления
     * returns:
     * template - не валидная форма
     * error - ошибка сервера
     * redirect - успешное создание
     *
     * @return mixed
     * @throws \Exception
     */
    public function createProcess()
    {
        if (($user_id = $this->model('users')->isAuth()) && ($this->model('adverts')->creatingAllowed($user_id))) {

            $validation = $this->service('validator')->createAdvertValidator($_POST);

            if (count($validation['error'])) {

                return $this->sendTemplate('create_advert', array_merge($this->buildFilterDictionaries(), $validation['error']));

            } elseif (count($validation['success'])) {
                $validation['success']['user_id'] = $this->model('users')->isAuth();
                $advert_id = $this->model('adverts')->create($validation['success']);
                for ($key = 0; $key < count($_FILES['photos']['name']); $key++) {
                    $file_ext = explode('.', $_FILES['photos']['name'][$key]);
                    $file_ext = array_pop($file_ext);
                    $new_name = $this->model('users')->isAuth() . '_' . time() . '_' . $key . '.' . $file_ext;
                    if ($file_ext && rename($_FILES['photos']['tmp_name'][$key], _ROOT_ . '/uploads/' . $new_name)) {
                        $this->model('adverts')->createFile([
                            'advert_id' => $advert_id,
                            'file_name' => $new_name,
                        ]);
                    }
                }
                $this->redirect(_SITE_NAME_ . '/');

            } else {
                $this->sendError(404);
            }


        } else {
            $this->redirect(_SITE_NAME_ . '/');
        }
    }

    /**
     * Попытка удаления объявления
     * returns redirect
     *
     * @param $id
     * @throws \Exception
     */
    public function delete($id)
    {
        if ($user_id = $this->model('users')->isAuth()) {
            $this->model('adverts')->delete($id, $user_id);
        }
        $this->redirect(_SITE_NAME_ . '/');

    }

    /**
     * Валидация данных фильтров и и получение результата в случае валидных фильтров
     * returns:
     * template - не валидная форма | отображение результатов
     * error - ошибка сервера
     *
     * @return mixed
     * @throws \Exception
     */
    public function search()
    {

        $validation = $this->service('validator')->searchAdvertsValidator($_POST);

        $filters_data = $this->service('filters')->buildSelectArrays();

        if (count($validation['error'])) {

            if ($user_id = $this->model('users')->isAuth()) {
                $user_adv = $this->model('adverts')->getUserAdverts($user_id);
            } else {
                $user_adv = [];
            }
            $add_icon = $this->model('adverts')->creatingAllowed($user_id);
            $last_adverts = $this->model('adverts')->getLastAdverts();
            $seen_adverts = $this->model('adverts')->getSeenAdverts();
            $params = ['add_icon' => $add_icon, 'user_adv' => $user_adv, 'last_adverts' => $last_adverts, 'seen_adverts' => $seen_adverts];


            $params = array_merge($filters_data, $params, $validation['error']);
            return $this->sendTemplate('index', $params);

        } elseif (count($validation['success'])) {

            setcookie("search_filters", json_encode($validation['success']), -1, "/");

            $search_condition = $this->service('filters')->createWhereString($validation['success']);

            $adverts = $this->model('adverts')->getFilterdAdverts($search_condition);
            $count = $this->model('adverts')->countSearchResult($search_condition);
            $pages_count = (int)($count / 10 + 1);
            $params = [
                'adverts' => $adverts,
                'count' => $count,
                'pages_count' => $pages_count,
            ];

            $params = array_merge($filters_data, $params, $validation['success']);
            return $this->sendTemplate('search_result', $params);


        } else {
            $this->sendError(404);
        }

    }

    /**
     * Отправка json строки данных одного объявление
     * returns json
     * @param $id - ID объявления
     * @return bool
     * @throws \Exception
     */
    public function show($id)
    {
        if ($this->service('validator')->isNumber($id)) {
            if ($advert = $this->model('adverts')->getAdvert($id)) {

                $seen_adv = (isset($_COOKIE['seen_adv'])) ? json_decode($_COOKIE['seen_adv']) : [];
                if (!in_array($id, $seen_adv)) {
                    array_push($seen_adv, $id);
                    if (count($seen_adv) == 6) {
                        array_shift($seen_adv);
                    }
                    setcookie("seen_adv", json_encode($seen_adv), -1, "/");
                }
                $this->sendJson(json_encode($advert));
                return true;
            }
        }
        $this->sendJson(json_encode([
            'error' => 1,
        ]));

    }

    /**
     * Переход на другую страницу результов поиска
     * Данные фильтров передаются из кук
     *
     * @param $page_id - ID страницы, на которую произошел переход
     * @return bool
     * @throws \Exception
     */
    public function setPage($page_id)
    {
        if ($this->service('validator')->isNumber($page_id) && $page_id) {

            $filters = json_decode($_COOKIE['search_filters'], true);
            $validation = $this->service('validator')->searchAdvertsValidator($filters);
            if (count($validation['success'])) {
                $search_condition = $this->service('filters')->createWhereString($validation['success']);

                $adverts = $this->model('adverts')->getFilterdAdverts($search_condition, ($page_id - 1));
                $count = $this->model('adverts')->countSearchResult($search_condition);
                $this->sendJson(json_encode(
                    [
                        'adverts' => $adverts,
                        'count_pages' => (int)($count / 10 + 1),
                    ]
                ));
                return true;
            }
        }
        $this->sendJson(json_encode([
            'error' => 1,
        ]));
    }

    /**
     * Получение и обработка данных из справочников для отображения фильтров
     *
     * @return array
     * @throws \Exception
     */
    private function buildFilterDictionaries()
    {

        if ($user_id = $this->model('users')->isAuth()) {
            $user_adv = $this->model('adverts')->getUserAdverts($user_id);
        } else {
            $user_adv = [];
        }

        return array_merge($this->service('filters')->buildSelectArrays(), ['user_adv' => $user_adv]);
    }

}