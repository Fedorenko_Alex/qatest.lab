<?php


namespace Controllers;


use Kernel\Controller;

class Welcome extends Controller
{

    /**
     * Рендер главной страницы ресурса
     * returns template
     *
     * @throws \Exception
     */
    public function index(){

        if ($user_id = $this->model('users')->isAuth()) {
            $user_adv = $this->model('adverts')->getUserAdverts($user_id);
        }else{
            $user_adv = [];
        }
        $add_icon = $this->model('adverts')->creatingAllowed($user_id);

        $filetrs_data = $this->service('filters')->buildSelectArrays();
        $last_adverts = $this->model('adverts')->getLastAdverts();

        $seen_adverts = $this->model('adverts')->getSeenAdverts();


        $params = ['add_icon'=>$add_icon, 'user_adv'=>$user_adv, 'last_adverts'=>$last_adverts, 'seen_adverts'=>$seen_adverts];

        $params = array_merge($filetrs_data, $params);


        return $this->sendTemplate('index', $params);
    }
}