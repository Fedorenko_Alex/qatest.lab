<?php


namespace Controllers;


use Kernel\Controller;
use Exception;

class Auth extends Controller
{

    /**
     * Ренлер страницы авторизации
     * returns template
     *
     * @return mixed
     * @throws Exception
     */
    public function signIn()
    {
        if (!$this->model('users')->isAuth()) {
            return $this->sendTemplate('login');
        } else {
            $this->redirect(_SITE_NAME_ . '/');
        }

    }

    /**
     * Обработка данных авторизации
     * returns:
     * template - не валидная форма
     * error - ошибка сервера
     * redirect - успешная авторизация
     * @throws Exception
     */
    public function signInProcess()
    {
        if (!$this->model('users')->isAuth()) {

            $validation = $this->service('validator')->loginValidator($_POST);
            if (count($validation['error'])) {

                return $this->sendTemplate('login', array_merge($validation['error'], $validation['success']));

            } elseif (count($validation['success'])) {

                $_SESSION['user_id'] = $validation['success']['id'];
                $this->redirect(_SITE_NAME_ . '/');

            } else {
                $this->sendError(404);
            }

        } else {
            $this->redirect(_SITE_NAME_ . '/');
        }
    }

    /**
     * Рендер формы регистрации
     * returns template
     *
     * @throws Exception
     */
    public function register()
    {
        if (!$this->model('users')->isAuth()) {
            $this->sendTemplate('register');
        } else {
            $this->redirect(_SITE_NAME_ . '/');
        }
    }

    /**
     * Обработка данных регистрации
     * returns:
     * template - не валидная форма
     * error - ошибка сервера
     * redirect - успешная регистрация
     *
     * @return mixed
     * @throws Exception
     */
    public function registerProcess()
    {
        if (!$this->model('users')->isAuth()) {

            $validation = $this->service('validator')->registerValidator($_POST);

            if (count($validation['error'])) {

                return $this->sendTemplate('register', array_merge($validation['error'], $validation['success']));

            } elseif (count($validation['success'])) {
                if ($user_id = $this->model('users')->createUser($validation['success'])) {
                    $_SESSION['user_id'] = $user_id;
                    $this->redirect(_SITE_NAME_ . '/');
                } else {
                    throw new Exception('User creating iternal error');
                }
            } else {
                $this->sendError(404);
            }


        } else {
            $this->redirect(_SITE_NAME_ . '/');
        }
    }

    /**
     * Вызод из системы
     * returns redirect
     *
     * @throws Exception
     */
    public function signOut()
    {
        if ($this->model('users')->isAuth()) {
            unset($_SESSION['user_id']);
        }
        $this->redirect(_SITE_NAME_ . '/');

    }

}