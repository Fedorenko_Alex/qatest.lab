<?php


namespace Models;


use Kernel\Model;

class Models extends Model
{

    /**
     * Получение перечня моделей авто в системе с указанными марками каждого
     *
     * @return array|bool
     */
    public function getAll(){
        $table = 'dictionary_models models';
        $columns = 'models.id,
                    models.name AS model,
                    marks.id AS mark_id,
                    marks.name AS mark';
        $join = 'INNER JOIN dictionary_marks marks ON models.mark_id = marks.id';
        return $this->DB->selectAllWithJoin($columns, $table, $join);
    }

    /**
     * Проверка наличие в справочнике модели с переданным id
     *
     * @param $model_id
     * @return bool
     */
    public function issetModel($model_id){
        return ($this->DB->selectOne('id', 'dictionary_models', 'id="' . $model_id . '"')) ? true : false;
    }

    /**
     * Проверка наличие в справочнике марки с переданным id
     *
     * @param $mark_id
     * @return bool
     */
    public function issetMark($mark_id){
        return ($this->DB->selectOne('id', 'dictionary_marks', 'id="' . $mark_id . '"')) ? true : false;
    }

}