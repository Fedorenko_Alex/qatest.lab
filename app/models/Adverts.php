<?php


namespace Models;


use Kernel\Model;

class Adverts extends Model
{

    /**
     * Получение данных объявление и перечня подгруженных фото в виде массива отдельным ключем
     *
     * @param $id - номер объявления
     * @return bool|mixed
     */
    public function getAdvert($id)
    {
        $table = 'adverts';
        $columns = 'adverts.id,
                    adverts.created_at,
                    cities.name AS city,
                    regions.name AS region,
                    models.name AS model,
                    marks.name AS mark,
                    adverts.engine_capacity,
                    adverts.mileage,
                    adverts.used,
                    photos.file_name as file_name';
        $join = 'INNER JOIN users ON adverts.user_id = users.id
                 INNER JOIN dictionary_cities cities ON adverts.city_id = cities.id
                 INNER JOIN dictionary_regions regions ON cities.region_id = regions.id
                 INNER JOIN dictionary_models models ON adverts.model_id = models.id
                 INNER JOIN dictionary_marks marks ON models.mark_id = marks.id
                 INNER JOIN photos ON photos.advert_id = adverts.id';
        $where = 'adverts.id = "' . $id . '" and adverts.status = 1';
        $adv = $this->DB->selectAllWithJoin($columns, $table, $join, $where);
        if (is_array($adv)) {
            $res = $adv[0];
            $res['photos'] = [];
            foreach ($adv as $val) {
                $res['photos'][] = $val['file_name'];
            }
            return $res;
        }
        return false;
    }

    /**
     * Получение переченя объявление конкретного пользователя. Для каждого объявления загружается название только первого фото
     * @param $userId
     * @return array|bool
     */
    public function getUserAdverts($userId)
    {
        $table = 'adverts';
        $columns = 'adverts.id,
                    adverts.created_at,
                    cities.name AS city,
                    regions.name AS region,
                    models.name AS model,
                    marks.name AS mark,
                    adverts.engine_capacity,
                    adverts.mileage,
                    adverts.used';
        $join = 'INNER JOIN users ON adverts.user_id = users.id
                 INNER JOIN dictionary_cities cities ON adverts.city_id = cities.id
                 INNER JOIN dictionary_regions regions ON cities.region_id = regions.id
                 INNER JOIN dictionary_models models ON adverts.model_id = models.id
                 INNER JOIN dictionary_marks marks ON models.mark_id = marks.id';
        $where = 'adverts.user_id = "' . $userId . '" and adverts.status = 1 order by adverts.created_at DESC';
        return $this->DB->selectAllWithJoin($columns, $table, $join, $where);
    }

    /**
     * Провекра количества объявлений пользователя и возврат разрешения на добавление.
     * @param $userId
     * @return bool
     */
    public function creatingAllowed($userId)
    {
        return ($this->DB->selectOne('COUNT(id) as count', 'adverts', 'user_id="' . $userId . '" and status = 1')['count'] < 3);
    }

    /**
     * Получение отфильтрованных объявлений. . Для каждого объявления загружается название только первого фото.
     *
     * @param $where_in - условие выборки, полученное Filters::createWhereString
     * @param int $page - количество страниц отступа при выборке данных
     * @return array|bool
     */
    public function getFilterdAdverts($where_in, $page = 0)
    {
        if ($page) {
            $page *= 10;
            $page = 'OFFSET ' . $page;
        } else {
            $page = '';
        }
        $table = 'adverts';
        $columns = 'adverts.id,
                    adverts.created_at,
                    cities.name AS city,
                    regions.name AS region,
                    models.name AS model,
                    marks.name AS mark,
                    adverts.engine_capacity,
                    adverts.mileage,
                    adverts.used,
                    (SELECT file_name from photos WHERE advert_id = adverts.id LIMIT 1) as file_name';
        $join = 'INNER JOIN users ON adverts.user_id = users.id
                 INNER JOIN dictionary_cities cities ON adverts.city_id = cities.id
                 INNER JOIN dictionary_regions regions ON cities.region_id = regions.id
                 INNER JOIN dictionary_models models ON adverts.model_id = models.id
                 INNER JOIN dictionary_marks marks ON models.mark_id = marks.id';
        $where = $where_in . ' and adverts.status = 1 order by adverts.created_at DESC LIMIT 10 ' . $page;
        return $this->DB->selectAllWithJoin($columns, $table, $join, $where);
    }

    /**
     * Получение количества объявлений, удовлетворяющих поисковому запросу
     *
     * @param $where_in - условие выборки, полученное Filters::createWhereString
     * @return mixed
     */
    public function countSearchResult($where_in)
    {
        $table = 'adverts';
        $columns = 'COUNT(adverts.id) as res';
        $join = 'INNER JOIN users ON adverts.user_id = users.id
                 INNER JOIN dictionary_cities cities ON adverts.city_id = cities.id
                 INNER JOIN dictionary_regions regions ON cities.region_id = regions.id
                 INNER JOIN dictionary_models models ON adverts.model_id = models.id
                 INNER JOIN dictionary_marks marks ON models.mark_id = marks.id';
        $where = $where_in . ' and adverts.status = 1';
        return $this->DB->selectOneWithJoin($columns, $table, $join, $where)['res'];
    }

    /**
     * Получение поледних 5 объявлений в системе. . Для каждого объявления загружается название только первого фото
     *
     * @return array|bool
     */
    public function getLastAdverts()
    {
        $table = 'adverts';
        $columns = 'adverts.id,
                    adverts.created_at,
                    cities.name AS city,
                    regions.name AS region,
                    models.name AS model,
                    marks.name AS mark,
                    adverts.engine_capacity,
                    adverts.mileage,
                    adverts.used,
                    (SELECT file_name from photos WHERE advert_id = adverts.id LIMIT 1) as file_name';
        $join = 'INNER JOIN users ON adverts.user_id = users.id
                 INNER JOIN dictionary_cities cities ON adverts.city_id = cities.id
                 INNER JOIN dictionary_regions regions ON cities.region_id = regions.id
                 INNER JOIN dictionary_models models ON adverts.model_id = models.id
                 INNER JOIN dictionary_marks marks ON models.mark_id = marks.id';
        $where = 'adverts.status = 1 order by adverts.created_at DESC LIMIT 5';
        return $this->DB->selectAllWithJoin($columns, $table, $join, $where);
    }

    /**
     * Получение данных объявлений, просмотренных пользователем. Пречень идентификаторов объявлений хранится в куках.
     *
     * @return array|bool
     */
    public function getSeenAdverts()
    {
        $seen_adv = (isset($_COOKIE['seen_adv'])) ? json_decode($_COOKIE['seen_adv']) : [];
        if (count($seen_adv)) {
            foreach($seen_adv as $key=> $adv){
                $seen_adv[$key] = (int)$adv;
            }
            $seen_adv = implode(',', $seen_adv);

        } else {
            return [];
        }
        $table = 'adverts';
        $columns = 'adverts.id,
                    adverts.created_at,
                    cities.name AS city,
                    regions.name AS region,
                    models.name AS model,
                    marks.name AS mark,
                    adverts.engine_capacity,
                    adverts.mileage,
                    adverts.used,
                    (SELECT file_name from photos WHERE advert_id = adverts.id LIMIT 1) as file_name';
        $join = 'INNER JOIN users ON adverts.user_id = users.id
                 INNER JOIN dictionary_cities cities ON adverts.city_id = cities.id
                 INNER JOIN dictionary_regions regions ON cities.region_id = regions.id
                 INNER JOIN dictionary_models models ON adverts.model_id = models.id
                 INNER JOIN dictionary_marks marks ON models.mark_id = marks.id';
        $where = '(adverts.id IN (' . $seen_adv . ')) and adverts.status = 1 order by adverts.created_at DESC LIMIT 5';
        return $this->DB->selectAllWithJoin($columns, $table, $join, $where);
    }

    /**
     * Создание нового объявления
     *
     * @param $data - асоциативный массив параметров
     * @return bool|string - ID новой записи | false
     */
    public function create($data)
    {
        return $this->DB->insert('adverts', $data);
    }

    /**
     * Установка в -1 статуса объявление, если переданный пользователб является его создателем
     *
     * @param $id
     * @param $user_id
     * @return bool
     */
    public function delete($id, $user_id)
    {
        return $this->DB->update('adverts', ['status' => -1], 'id="' . $id . '" and user_id="' . $user_id . '"');
    }

    /**
     * Обновление данных объявления
     *
     * @param $id
     * @param $data - асоциативный массив новых параметров
     * @return bool
     */
    public function update($id, $data)
    {
        return $this->DB->update('adverts', $data, 'id="' . $id . '"');
    }

    /**
     * Создание записи о загруженном фото
     *
     * @param $data
     * @return bool|string
     */
    public function createFile($data)
    {
        return $this->DB->insert('photos', $data);
    }
}