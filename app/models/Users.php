<?php


namespace Models;


use Kernel\Model;

class Users extends Model
{

    /**
     * Проверка сессии на предмет наличия идентификатора авторизированного пользователя
     *
     * @return bool|mixed - ID пользователя | false
     */
    public function isAuth()
    {
        if (isset($_SESSION['user_id']) && $_SESSION['user_id'] > 0) {
            return $_SESSION['user_id'];
        }
        return false;
    }

    /**
     * Проверка наличия в системе пользователя с переданным email
     *
     * @param $email
     * @return bool
     */
    public function issetEmail($email)
    {
        return ($this->DB->selectOne('id', 'users', 'email="' . $email . '"')) ? true : false;
    }

    /**
     * Получение email пользователя по его ID
     *
     * @param $user_id
     * @return bool
     */
    public function getEmail($user_id){
        $res = ($this->DB->selectOne('email', 'users', 'id="' . $user_id . '"'));
        if($res)
            return $res['email'];
        return false;
    }

    /**
     * Получение данных пользователя по его ID
     *
     * @param $email
     * @return bool|mixed
     */
    public function getUser($email){
        return $this->DB->selectOne('*', 'users', 'email="' . $email . '"');
    }

    /**
     * Создание нового пользователя в системе
     *
     * @param $data - асоциативный массив параметров
     * @return bool|string - ID новой записи | false
     */
    public function createUser($data)
    {
        return $this->DB->insert('users',$data);
    }
}