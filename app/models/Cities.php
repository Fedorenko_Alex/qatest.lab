<?php


namespace Models;


use Kernel\Model;

class Cities extends Model
{

    /**
     * Получение перечня городов в системе с указанными областями каждого
     *
     * @return array|bool
     */
    public function getAll(){
        $table = 'dictionary_cities cities';
        $columns = 'cities.id,
                    cities.name AS city,
                    regions.id AS region_id,
                    regions.name AS region';
        $join = 'INNER JOIN dictionary_regions regions ON cities.region_id = regions.id';
        return $this->DB->selectAllWithJoin($columns, $table, $join);
    }

    /**
     * Проверка наличие в справочнике города с переданным id
     *
     * @param $city_id
     * @return bool
     */
    public function issetCity($city_id){
        return ($this->DB->selectOne('id', 'dictionary_cities', 'id="' . $city_id . '"')) ? true : false;
    }

    /**
     * Проверка наличие в справочнике области с переданным id
     *
     * @param $region_id
     * @return bool
     */
    public function issetRegion($region_id){
        return ($this->DB->selectOne('id', 'dictionary_regions', 'id="' . $region_id . '"')) ? true : false;
    }

}