<?php
global $configuration;

$configuration = [
    'display_error' => 1,
    'db' => [
        'host' => 'localhost',
        'port' => 3306,
        'base' => 'qatestdb',
        'user' => 'root',
        'pass' => '',
    ],
    'site_name' => 'qatest.lab'

];