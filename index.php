<?php
require "vendor/autoload.php";

/** @var string _ROOT_ Серверный путь к рабочей папке */
define('_ROOT_', __DIR__);
/** @var string _WEB_ROOT_ WEB путь к рабочей папке */
define('_WEB_ROOT_', str_replace($_SERVER['DOCUMENT_ROOT'], '', _ROOT_));
/** @var string _UPLOAD_ путь к папке с загруженными файлами */
define('_UPLOAD_', _ROOT_ . '/uploads');
/** @var string _WEB_ROOT_UPLOAD_ WEB путь к папке с загруженными файлами */
define('_WEB_UPLOAD_', _WEB_ROOT_ . '/uploads');

require_once(_ROOT_ . '/config/env.php');

define('_SITE_NAME_', $GLOBALS['configuration']['site_name']);

ini_set('error_reporting', E_ALL & ~E_DEPRECATED);
ini_set('display_errors', $GLOBALS['configuration']['display_error']);

require_once(_ROOT_ . '/kernel/system_func.php');

new Kernel\App();