let current_page;

function handleError($type, $text) {
    let error_markup = ' <div class="container js_alert mt-3">' +
        '<div class="alert alert-' + $type + '" role="alert">' +
        $text +
        '</div>' +
        '</div>';
    $(".navbar").after(error_markup);
    setTimeout(function () {
        $('.js_alert').remove();
    }, 5000);
}

$(document).delegate('.custom-file-input', 'change', function (event) {

    let availableFileTypes = ['image/jpeg', 'image/png'];

    if (event.target.files.length > 3) {
        handleError('danger', 'До оголошення дозволено додавати лише 3 фото!')
    } else {
        let files_name = '';
        $.each(event.target.files, function (key, val) {
            if (availableFileTypes.indexOf(val.type) === -1) {
                handleError('danger', 'Завантажувати дозволено лише зображення');
                delete (event.target.files);
                return false;
            }
            files_name += val.name + ', ';

        });
        $(this).next('.custom-file-label').html(files_name);
        delete (event.target.files);
    }

});

$(document).delegate('input.is-invalid', 'focus', function () {
    $(this).removeClass('is-invalid');
    $(this).next(".text-danger").remove();
});

$(document).delegate('[data-url]', 'click', function (e) {

    e.preventDefault();

    window.location.href = $(this).attr('data-url');

});


$(document).delegate('#regions', 'change', function () {
    let region = $('#regions').val();
    let city = $('#cities option');
    if (region == 0) {
        city.show();
    } else {
        city.show();
        $('#cities option[data-region != ' + region + ']').hide();
        $('#cities option[value = 0]').show();
        $('#cities').val(0);
        if ($('#cities option[data-region = ' + region + ']')[0]) {
            $('#cities').val($('#cities option[data-region = ' + region + ']:first').val());
        }
    }
});

$(document).delegate('#cities', 'change', function () {
    let region = $('#cities option:selected').attr('data-region');
    if (region != undefined) {
        $('#regions').val(region);
    }
});

$(document).delegate('#marks', 'change', function () {
    let mark = $('#marks').val();
    let city = $('#models option');
    if (mark == 0) {
        city.show();
    } else {
        city.show();
        $('#models option[data-mark != ' + mark + ']').hide();
        $('#models option[value = 0]').show();
        $('#models').val(0);
        if ($('#models option[data-mark = ' + mark + ']')[0]) {
            $('#models').val($('#models option[data-mark = ' + mark + ']:first').val());
        }
    }
});

$(document).delegate('#models', 'change', function () {
    let mark = $('#models option:selected').attr('data-mark');
    if (mark != undefined) {
        $('#marks').val(mark);
    }
});

function setModel(model_id) {
    $('#models').val(model_id);
    let mark = $('#models option:selected').attr('data-mark');
    if (mark != undefined) {
        $('#marks').val(mark);
    }
}

function setCity(city_id) {
    $('#cities').val(city_id);
    let region = $('#cities option:selected').attr('data-region');
    if (region != undefined) {
        $('#regions').val(region);
    }
}

function setRegion(region_id) {
    $('#regions').val(region_id);
}

function setMark(mark_id) {
    $('#marks').val(mark_id);
}

$(document).delegate('[data-advert]', 'click', function (e) {
    e.preventDefault();
    let advert_id = $(this).attr('data-advert');
    $.get('/posters/show/' + advert_id, {}, function (answer) {
        let content = '';
        if (answer.error !== undefined) {
            content = '<span class="text-danger">При завантаженні даних сталася помилка</span>';
        } else {
            content += '<div class="modal-header">\n' +
                '           <h5 class="modal-title" id="exampleModalLabel">' + answer.mark + ' ' + answer.model + '</h5>\n' +
                '           <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
                '               <span aria-hidden="true">&times;</span>\n' +
                '           </button>\n' +
                '       </div>\n' +
                '       <div class="modal-body">' +
                '           <div id="carouselModalIndicators" class="carousel slide" data-ride="carousel">\n' +
                '               <ol class="carousel-indicators">\n';


            answer.photos.forEach(function (item, index, array) {
                content += '<li data-target="#carouselModalIndicators" data-slide-to="' + index + '" class="' + ((index === 0) ? 'active' : '') + '"></li>\n'
            });


            content += ' </ol>\n' +
                '    <div class="carousel-inner">\n';

            answer.photos.forEach(function (item, index, array) {
                content += '<div class="carousel-item carousel-image ' + ((index === 0) ? 'active' : '') + '" style="background-image: url(\'../uploads/' + item + '\');">\n' +
                    '           <div class="carousel-caption d-none d-md-block">\n' +
                '                   <p>' + ((answer.used === 0) ? 'Нова' : 'Вживана') + ' Об’єм двигуна (л.): ' + answer.engine_capacity + ' Пробіг (тис. км): ' + answer.mileage + ' ' + answer.region + ' ' + answer.city + '</p>\n' +
                '           </div>\n' +
                '       </div>\n';
            });

            content += '</div>\n' +
                '<a class="carousel-control-prev" href="#carouselModalIndicators" role="button" data-slide="prev">\n' +
                '    <span class="carousel-control-prev-icon" aria-hidden="true"></span>\n' +
                '    <span class="sr-only">Previous</span>\n' +
                '</a>\n' +
                '<a class="carousel-control-next" href="#carouselModalIndicators" role="button" data-slide="next">\n' +
                '    <span class="carousel-control-next-icon" aria-hidden="true"></span>\n' +
                '    <span class="sr-only">Next</span>\n' +
                '</a>\n' +
                '</div>' +
                '</div>';
        }
        $('#universalModal .modal-content').html(content);
        $('#universalModal').modal('show');
    });

});

$(document).delegate('[data-page]', 'click', function (e) {
    e.preventDefault();
    let type = $(this).attr('data-page');

    if (type === 'next') {
        current_page++;
    } else if (type === 'prev') {
        current_page--;
    }

    $.get('/posters/setPage/' + current_page, {}, function (answer) {
        if (answer.error === undefined) {
            let content = '';
            answer.adverts.forEach(function (item, index, array) {
                content += ' <a href="#" class="list-group-item list-group-item-action d-flex flex-row"\n' +
                    '                               data-advert="' + item.id + '">\n' +
                    '                                <img src="../uploads/' + item.file_name + '" class="align-self-center mr-3 pull-left"\n' +
                    '                                     width="128" alt="...">\n' +
                    '                                <div class="d-flex flex-column w-100">\n' +
                    '                                    <div class="">\n' +
                    '                                        <h5 class="mb-1 pull-left">' + item.mark + ' ' + item.model + '</h5>\n' +
                    '                                        <small class="pull-right">' + item.created_at + '</small>\n' +
                    '                                    </div>\n' +
                    '                                    <p class="mb-1">' + ((item.id) ? 'Вживана' : 'Нова') + '</p>\n' +
                    '                                    <p class="mb-1">Об\'єм двигуна (л.): ' + item.engine_capacity + '</p>\n' +
                    '                                    <p class="mb-1">Пробіг (тис. км): ' + item.mileage + '</p>\n' +
                    '                                    <small>' + item.region + ' ' + item.city + '</small>\n' +
                    '                                </div>\n' +
                    '                            </a>';
            });

            $('.list-group-flush').html(content);

            if (current_page === 1 && $('#prevPage').length > 0) {
                $('#prevPage').remove();
            } else if (current_page > 1 && $('#prevPage').length === 0) {
                let prev_page = '<li class="page-item" id="prevPage"><a class="page-link" href="#" data-page="prev"> << Попередня </a></li>';
                $('#paginator').prepend(prev_page);
            }

            if (current_page === answer.count_pages && $('#nextPage').length > 0) {
                $('#nextPage').remove();
            } else if (current_page < answer.count_pages && $('#nextPage').length === 0) {
                let next_page = '<li class="page-item" id="nextPage"><a class="page-link" href="#" data-page="next"> Наступна >> </a></li>';
                $('#paginator').append(next_page);
            }
        }
    });

});