<?php


namespace DataProviders;


use Exception;
use \PDO;

class MysqlDB
{
    private $conn;


    private $debug;


    function __construct()
    {

        try {
            $this->debug = $GLOBALS['configuration']['display_error'];

            $conn = new PDO("mysql:host=" . $GLOBALS['configuration']['db']['host'] . ";port=" . $GLOBALS['configuration']['db']['port'] . ";dbname=" . $GLOBALS['configuration']['db']['base'], $GLOBALS['configuration']['db']['user'], $GLOBALS['configuration']['db']['pass']);

            if ($this->debug) {
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            $conn->query("SET character_set_connection = 'utf8'");
            $conn->query("SET character_set_client = 'utf8'");
            $conn->query("SET character_set_results = 'utf8'");
            $this->conn = $conn;
        } catch (PDOException $e) {
            $this->onError(__FUNCTION__, $e->getMessage());
        }
    }

    private function query($sql)
    {
        try {
            return $this->conn->query($sql);
        } catch (Exception $e) {
            $this->onError(__FUNCTION__, $e->getMessage());
        }
    }

    public function selectOne($col, $table, $where = '')
    {
        if (isset($col) && $col != "" && isset($table) && $table != "") {
            $where = (!empty($where)) ? " WHERE " . $where : " WHERE 1";
            if (strpos($where, 'LIMIT') === false) {
                $where .= ' LIMIT 1';
            }
            try {
                $result = $this->query("SELECT " . $col . " FROM " . $table . $where);
                if ($result) {
                    return $result->fetch(PDO::FETCH_ASSOC);
                }
            } catch (PDOException $e) {
                if ($this->debug) {
                    $this->onError(__FUNCTION__, $e->getMessage());
                }
            }
        } else {
            $this->onError(__FUNCTION__, 'There are no required parameters.');
        }
        return false;
    }

    public function selectAll($col, $table, $where = '')
    {
        if (isset($col) && $col != "" && isset($table) && $table != "") {
            $where = (!empty($where)) ? " WHERE " . $where : " WHERE 1";
            try {
                $result = $this->query("SELECT " . $col . " FROM " . $table . $where);
                if ($result) {
                    return $result->fetchAll(PDO::FETCH_ASSOC);
                }
            } catch (PDOException $e) {
                if ($this->debug) {
                    $this->onError(__FUNCTION__, $e->getMessage());
                }
            }
        } else {
            $this->onError(__FUNCTION__, 'There are no required parameters.');
        }
        return false;
    }

    public function selectAllWithJoin($col, $table, $join = '', $where = '', $orderBy = '')
    {
        if (isset($col) && $col != "" && isset($table) && $table != "") {
            if ($where)
                $where = (!empty($where)) ? " WHERE " . $where : " WHERE 1";
            try {
                $queryString = "SELECT " . $col . " FROM " . $table . ' ' . $join . $where . $orderBy;
                $result = $this->query($queryString);
                if ($result) {
                    return $result->fetchAll(PDO::FETCH_ASSOC);
                }
            } catch (PDOException $e) {
                if ($this->debug) {
                    $this->onError(__FUNCTION__, $e->getMessage());
                }
            }
        } else {
            $this->onError(__FUNCTION__, 'There are no required parameters.');
        }
        return false;
    }

    public function selectOneWithJoin($col, $table, $join = '', $where = '')
    {
        if (isset($col) && $col != "" && isset($table) && $table != "") {
            $where = (!empty($where)) ? " WHERE " . $where : " WHERE 1";
            try {
                $result = $this->query("SELECT " . $col . " FROM " . $table . ' ' . $join . $where);
                if ($result) {
                    return $result->fetch(PDO::FETCH_ASSOC);
                }
            } catch (PDOException $e) {
                if ($this->debug) {
                    $this->onError(__FUNCTION__, $e->getMessage());
                }
            }
        } else {
            $this->onError(__FUNCTION__, 'There are no required parameters.');
        }
        return false;
    }

    public function insert($table, $params)
    {
        if (!empty($table)) {
            $insert = true;
            if (is_array($params) && ($count = count($params)) > 0) {
                $i = 1;
                $set = '';
                foreach ($params as $k => $v) {
                    $coma = ($count != $i) ? "," : "";
                    if (is_null($v))
                        $set .= "`" . $k . "`=" . "NULL" . $coma;
                    else
                        $set .= "`" . $k . "`=" . $this->conn->quote($v) . $coma;
                    $i++;


                }
            } elseif (!empty($params)) {
                $set = $params;
            } else {
                $insert = false;
                $this->onError(__FUNCTION__, 'Empty data for insert.');
            }
            if ($insert) {
                $sql = "INSERT INTO " . $table . " SET " . $set;
                try {
                    if ($this->query($sql)) {
                        return $this->conn->lastInsertId();
                    }
                } catch (PDOException $e) {
                    echo 'INS' . $e;
                    if ($this->debug) {
                        $this->onError(__FUNCTION__, $e->getMessage());
                    }
                }
            }
        } else {
            $this->onError(__FUNCTION__, 'Empty name table,');
        }
        return false;
    }

    public function update($table, $params, $where)
    {
        if (!empty($table)) {
            if (!empty($where)) {
                $update = true;
                if (is_array($params) && ($count = count($params)) > 0) {
                    $i = 1;
                    $set = '';
                    foreach ($params as $k => $v) {

                        $coma = ($count != $i) ? "," : "";
                        if (is_null($v))
                            $set .= "`" . $k . "` = " . "NULL" . "" . $coma;
                        else
                            $set .= "`" . $k . "` = " . $this->conn->quote($v) . "" . $coma;
                        $i++;
                    }
                } elseif (!empty($params)) {
                    $set = $params;
                } else {
                    $update = false;
                    $this->onError(__FUNCTION__, 'Empty data for update.');
                }
                if ($update) {
                    $sql = "UPDATE " . $table . " SET " . $set . " WHERE " . $where;
                    try {
                        $result = $this->query($sql);
                        if ($result) {
                            return true;
                            //return $result->rowCount();
                        }
                    } catch (PDOException $e) {
                        echo 'UPD' . $e;
                        if ($this->debug) {
                            $this->onError(__FUNCTION__, $e->getMessage());
                        }
                    }
                }
            } else {
                $this->onError(__FUNCTION__, 'Empty where for update.');
            }
        } else {
            $this->onError(__FUNCTION__, 'Empty name table.');
        }
        return false;
    }

    public function delete($table, $where)
    {
        if (!empty($where)) {
            $where = " WHERE " . $where;
        } else {
            return false;
        }
        try {
            $result = $this->query("DELETE FROM " . $table . $where);
            if ($result) {
                return true;
            }
        } catch (PDOException $e) {
            if ($this->debug) {
                $this->onError(__FUNCTION__, $e->getMessage());
            }
        }

        return false;
    }

    private function onError($method, $message)
    {

        // Попытка получения ошибки от PDO
        if ($this->conn) {
            $error = $this->conn->errorInfo();
            if (isset($error[2]) && $error[2] != '') {
                // Переназначение сообщения об ошибке
                $message = $error[2] . '. (' . $message . ')';
            }
        }

        __error_log('sql_error', __CLASS__, $method, $message);


    }
}