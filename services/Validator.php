<?php


namespace Services;

use Models\Cities;
use Models\Models;
use Models\Users;

class Validator
{
    //Поля для сохранения экземпляров моделей и доступа к ним
    private $user_model;
    private $cities_model;
    private $models_model;


    public function __construct(array $models)
    {
        $this->user_model = $models['users'];
        $this->cities_model = $models['cities'];
        $this->models_model = $models['models'];
    }

    /**
     * Валидация формы регистрации
     *
     * @param $data - данные формы
     * @return array|bool - ['error' - поля не прошедшие валидацию и тексты ошибок; 'success' - поля, прошедшие валидацию]; false  - возникновение ошибки
     */
    public function registerValidator($data)
    {

        $errors = [];
        $success = [];


        if (isset($data['email'])) {
            if ($data['email'] == '') {
                $errors['email_error'] = 'Поле обов\'язкове для заповнення';
                $errors['email'] = $data['email'];
            } elseif (!$this->isMail($data['email'])) {
                $errors['email_error'] = 'Поле має некоректний формат';
                $errors['email'] = $data['email'];
            } elseif ($this->user_model->issetEmail($data['email'])) {
                $errors['email_error'] = 'Email адреса зареєстрована в системі';
                $errors['email'] = $data['email'];
            } else {
                $success['email'] = $data['email'];
            }
        } else {
            return false;
        }

        if (isset($data['password'])) {
            if ($data['password'] == '') {
                $errors['password_error'] = 'Поле обов\'язкове для заповнення';
            } elseif (mb_strlen($data['password']) < 8 || mb_strlen($data['password']) > 16) {
                $errors['password_error'] = 'Поле має некоректний формат';
            } else {
                $success['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            }
        } else {
            return false;
        }

        return [
            'error' => $errors,
            'success' => $success,
        ];

    }

    /**
     * Валидация формы входа
     *
     * @param $data - данные формы
     * @return array|bool - ['error' - поля не прошедшие валидацию и тексты ошибок; 'success' - поля, прошедшие валидацию]; false  - возникновение ошибки
     */
    public function loginValidator($data)
    {
        $errors = [];
        $success = [];


        if (isset($data['email'])) {
            if ($data['email'] == '') {
                $errors['email_error'] = 'Поле обов\'язкове для заповнення';
                $errors['email'] = $data['email'];
            } elseif (!$this->isMail($data['email'])) {
                $errors['email_error'] = 'Поле має некоректний формат';
                $errors['email'] = $data['email'];
            } elseif (!$this->user_model->issetEmail($data['email'])) {
                $errors['email_error'] = 'Email адресу не знайдено в системі';
                $errors['email'] = $data['email'];
            }else{
                $success['email'] = $data['email'];
            }
        } else {
            return false;
        }

        if (isset($data['password'])) {
            if ($data['password'] == '') {
                $errors['password_error'] = 'Поле обов\'язкове для заповнення';
            } elseif (mb_strlen($data['password']) < 8 || mb_strlen($data['password']) > 16) {
                $errors['password_error'] = 'Поле має некоректний формат';
            }
        } else {
            return false;
        }
        if (!count($errors)) {
            $user = $this->user_model->getUser($data['email']);
            if (password_verify($data['password'], $user['password'])) {
                $success['id'] = $user['id'];
            } else {
                $errors['email_error'] = 'Користувача з вказаними реквызитами не знайдено в системі';
                $errors['email'] = $data['email'];
            }

        }
        return [
            'error' => $errors,
            'success' => $success,
        ];
    }

    /**
     * Валидация формы создание объявления
     *
     * @param $data
     * @return array|bool  - ['error' - поля не прошедшие валидацию и тексты ошибок ; 'success' - поля, прошедшие валидацию]; false  - возникновение ошибки
     */
    public function createAdvertValidator($data)
    {
        $errors = [];
        $success = [];

        if (isset($data['used']) && $data['used'] === "1") {
            $success['used'] = 1;
        }else{
            $success['used'] = 0;
        }

        if (isset($data['city_id'])) {
            if ($data['city_id'] == '') {
                $errors['city_id_error'] = 'Поле обов\'язкове для заповнення';
            } elseif (!$this->isNumber($data['city_id'])) {
                $errors['city_id_error'] = 'Поле має некоректний формат';
            } elseif (!$this->cities_model->issetCity($data['city_id'])) {
                $errors['city_id_error'] = 'Поле має некоректний формат';
            } else {
                $success['city_id'] = $data['city_id'];
            }
        } else {
            return false;
        }

        if (isset($data['model_id'])) {
            if ($data['model_id'] == '') {
                $errors['model_id_error'] = 'Поле обов\'язкове для заповнення';
            } elseif (!$this->isNumber($data['model_id'])) {
                $errors['model_id_error'] = 'Поле має некоректний формат';
            } elseif (!$this->models_model->issetModel($data['model_id'])) {
                $errors['model_id_error'] = 'Поле має некоректний формат';
            } else {
                $success['model_id'] = $data['model_id'];
            }
        } else {
            return false;
        }

        if (isset($data['engine_capacity'])) {
            if ($data['engine_capacity'] == '') {
                $errors['engine_capacity_error'] = 'Поле обов\'язкове для заповнення';
            } elseif (!$this->isDecimal($data['engine_capacity'])) {
                $errors['engine_capacity_error'] = 'Поле має некоректний формат';
            } else {
                $success['engine_capacity'] = str_replace(',', '.', $data['engine_capacity']);
            }
        } else {
            return false;
        }

        if (isset($data['mileage'])) {
            if ($data['mileage'] == '') {
                $errors['mileage_error'] = 'Поле обов\'язкове для заповнення';
            } elseif (!$this->isNumber($data['mileage'])) {
                $errors['mileage_error'] = 'Поле має некоректний формат';
            } else {
                $success['mileage'] = $data['mileage'];
            }
        } else {
            return false;
        }

        if (!isset($_FILES['photos']) || $_FILES['photos']['name'][0] === '') {
            $errors['photos_error'] = 'Поле обов\'язкове для заповнення';
        } elseif (count($_FILES['photos']['name']) > 3) {
            $errors['photos_error'] = 'Дозволено завантажувати не більше 3 фотографій';
        } else {
            for ($key = 0; $key < count($_FILES['photos']['name']); $key++) {
                if ($_FILES['photos']['tmp_name'][$key] == '' || !is_file($_FILES['photos']['tmp_name'][$key])) {
                    $errors['photos_error'] = 'При завантаженні фотографій сталась помилка';
                } elseif (!$this->inDocumentMimeAllowed(mime_content_type($_FILES['photos']['tmp_name'][$key]))) {
                    $errors['photos_error'] = 'Дозволено завантажувати лише файли формату jpg та png';
                }
            }
        }

        if(count($errors)){
            $errors = array_merge($errors, $data);
        }

        return [
            'error' => $errors,
            'success' => $success,
        ];


    }

    /**
     * Валидация формы поиска объявлений
     *
     * @param $data - данные формы
     * @return array|bool - ['error' - поля не прошедшие валидацию и тексты ошибок; 'success' - поля, прошедшие валидацию]; false  - возникновение ошибки
     */
    public function searchAdvertsValidator($data)
    {
        $errors = [];
        $success = [];

        if (isset($data['used']) && $data['used'] === "1") {
            $success['used'] = 1;
        }elseif(isset($data['used']) && $data['used'] === "0"){
            $success['used'] = 0;
        }

        if (isset($data['city_id'])) {
            if ($data['city_id'] == '') {

            } elseif (!$this->isNumber($data['city_id'])) {
                $errors['city_id_error'] = 'Поле має некоректний формат';
            } elseif ($data['city_id'] != 0 && !$this->cities_model->issetCity($data['city_id'])) {
                $errors['city_id_error'] = 'Поле має некоректний формат';
            } else {
                $success['city_id'] = $data['city_id'];
            }
        }

        if (isset($data['region_id'])) {
            if ($data['region_id'] == '') {

            } elseif (!$this->isNumber($data['region_id'])) {
                $errors['region_id_error'] = 'Поле має некоректний формат';
            } elseif ($data['region_id'] != 0 && !$this->cities_model->issetRegion($data['region_id'])) {
                $errors['region_id_error'] = 'Поле має некоректний формат';
            } else {
                $success['region_id'] = $data['region_id'];
            }
        }

        if (isset($data['model_id'])) {
            if ($data['model_id'] == '') {

            } elseif (!$this->isNumber($data['model_id'])) {
                $errors['model_id_error'] = 'Поле має некоректний формат';
            } elseif ($data['model_id'] != 0 && !$this->models_model->issetModel($data['model_id'])) {
                $errors['model_id_error'] = 'Поле має некоректний формат';
            } else {
                $success['model_id'] = $data['model_id'];
            }
        }

        if (isset($data['mark_id'])) {
            if ($data['mark_id'] == '') {

            } elseif (!$this->isNumber($data['mark_id'])) {
                $errors['mark_id_error'] = 'Поле має некоректний формат';
            } elseif ($data['mark_id'] != 0 && !$this->models_model->issetMark($data['mark_id'])) {
                $errors['mark_id_error'] = 'Поле має некоректний формат';
            } else {
                $success['mark_id'] = $data['mark_id'];
            }
        }


        if (isset($data['engine_capacity_from'])) {
            if ($data['engine_capacity_from'] == '') {

            } elseif (!$this->isDecimal($data['engine_capacity_from'])) {
                $errors['engine_capacity_from_error'] = 'Поле має некоректний формат';
            } else {
                $success['engine_capacity_from'] = str_replace(',', '.', $data['engine_capacity_from']);
            }
        }

        if (isset($data['engine_capacity_to'])) {
            if ($data['engine_capacity_to'] == '') {

            } elseif (!$this->isDecimal($data['engine_capacity_to'])) {
                $errors['engine_capacity_to_error'] = 'Поле має некоректний формат';
            } else {
                $success['engine_capacity_to'] = str_replace(',', '.', $data['engine_capacity_to']);
            }
        }

        if (isset($data['mileage_from'])) {
            if ($data['mileage_from'] == '') {

            } elseif (!$this->isNumber($data['mileage_from'])) {
                $errors['mileage_from_error'] = 'Поле має некоректний формат';
            } else {
                $success['mileage_from'] = $data['mileage_from'];
            }
        }

        if (isset($data['mileage_to'])) {
            if ($data['mileage_to'] == '') {

            } elseif (!$this->isNumber($data['mileage_to'])) {
                $errors['mileage_to_error'] = 'Поле має некоректний формат';
            } else {
                $success['mileage_to'] = $data['mileage_to'];
            }
        }

        if(count($errors)){
            $errors = array_merge($errors, $data);
        }

        return [
            'error' => $errors,
            'success' => $success,
        ];


    }

    /**
     * Проверка входного параметра на положительное целое число
     *
     * @param $in
     * @return bool
     */
    public function isNumber($in)
    {
        $in = trim($in);
        if (preg_match("/[^0-9]/", $in)) {

            return false;

        }
        return true;
    }

    /**
     *  Проверка входного параметра на положительное десятичное число
     *
     * @param $in
     * @return bool
     */
    public function isDecimal($in)
    {
        $in = trim($in);
        $in = str_replace(',', '.', $in);
        $in = str_replace(' ', '', $in);
        if (preg_match("/[^0-9\.]/", $in)) {

            return false;

        }
        return true;
    }

    /**
     * Проверка входного параметра на соответствие регулярному выражению email типа
     *
     * @param $in
     * @return bool
     */
    public function isMail($in)
    {

        $explode = explode('@', $in);

        if (count($explode) != 2) {

            return false;

        }

        if ($explode[0] == '' || $explode[1] == '') {

            return false;

        }

        if (preg_match("/[^0-9a-zA-Z\.\-\_]/", $explode[0])) {

            return false;

        }

        if (strlen($explode[0]) > 64) {

            return false;

        }

        if (strlen($explode[1]) > 255) {

            return false;

        }

        $explode2 = explode('.', $explode[1]);
        $count = count($explode2);

        if ($count < 2) {

            return false;

        }

        if ($explode2[0] == '' || $explode2[1] == '') {

            return false;

        }

        if ($count == 2) {

            if (preg_match("/[^0-9a-zA-Z\-\_]/", $explode2[0])) {

                return false;

            }

            if (preg_match("/[^a-zA-Z]/", $explode2[1])) {

                return false;

            }

        } elseif ($count == 3) {

            if ($explode2[2] == '') {

                return false;

            }

            if (preg_match("/[^0-9a-zA-Z\-\_]/", $explode2[0])) {

                return false;

            }

            if (preg_match("/[^0-9a-zA-Z\-\_]/", $explode2[1])) {

                return false;

            }

            if (preg_match("/[^a-zA-Z]/", $explode2[2])) {

                return false;

            }

        } else {

            return false;

        }

        return true;

    }

    /**
     * Проверка mime типа документа на вхождение в список разрешенных для загрузки типов
     *
     * @param $mime
     * @return bool
     */
    private function inDocumentMimeAllowed($mime)
    {
        return in_array($mime, [
            'image/jpeg',
            'image/jpg',
            'image/png',
        ]);
    }


}