<?php


namespace Services;


class Filters
{

    private $user_model;
    private $cities_model;
    private $models_model;


    public function __construct(array $models)
    {
        $this->user_model = $models['users'];
        $this->cities_model = $models['cities'];
        $this->models_model = $models['models'];
    }

    /**
     * Получение из справочников информации по регионам/городам и маркам/моделям авто и формирование массивов для селектов
     *
     * @return array - масссив с ключами под каждый select
     */
    public function buildSelectArrays()
    {

        $cities_joined = $this->cities_model->getAll();
        $models_joined = $this->models_model->getAll();
        $cities = [];
        $regions = [];
        foreach ($cities_joined as $val) {
            if (!isset($regions[$val['region_id']])) {
                $regions[$val['region_id']] = $val['region'];
            }
            $cities[$val['id']] = [
                'name' => $val['city'],
                'region_id' => $val['region_id'],
            ];
        }

        $models = [];
        $marks = [];
        foreach ($models_joined as $val) {
            if (!isset($marks[$val['mark_id']])) {
                $marks[$val['mark_id']] = $val['mark'];
            }
            $models[$val['id']] = [
                'name' => $val['model'],
                'mark_id' => $val['mark_id'],
            ];
        }

        return ['regions' => $regions, 'cities' => $cities, 'marks' => $marks, 'models' => $models];;
    }

    /**
     * Формирование условия поиска объявлений на основании валидных данных запроса
     *
     * @param $data - массив с ключами, соответствующими значениям фильтров
     * @return string
     */
    public function createWhereString($data)
    {
        $where = '';
        $conditions = [];

        if (isset($data['used'])) {
            $conditions[] = 'adverts.used=' . $data['used'];
        }
        if (isset($data['mark_id']) and $data['mark_id']>0) {
            $conditions[] = 'models.mark_id=' . $data['mark_id'];
        }
        if (isset($data['model_id']) and $data['model_id']>0) {
            $conditions[] = 'adverts.model_id=' . $data['model_id'];
        }
        if (isset($data['region_id'])  and $data['region_id']>0) {
            $conditions[] = 'cities.region_id=' . $data['region_id'];
        }
        if (isset($data['city_id'])  and $data['city_id']>0) {
            $conditions[] = 'adverts.city_id=' . $data['city_id'];
        }

        if(isset($data['engine_capacity_from'])  and isset($data['engine_capacity_to'])){
            $conditions[] = 'adverts.engine_capacity BETWEEN ' . $data['engine_capacity_from']. ' AND '. $data['engine_capacity_to'];
        }elseif(isset($data['engine_capacity_from'])  and !isset($data['engine_capacity_to'])){
            $conditions[] = 'adverts.engine_capacity > ' . $data['engine_capacity_from'];
        }elseif(!isset($data['engine_capacity_from'])  and isset($data['engine_capacity_to'])){
            $conditions[] = 'adverts.engine_capacity < ' . $data['engine_capacity_to'];
        }

        if(isset($data['mileage_from'])  and isset($data['mileage_to'])){
            $conditions[] = 'adverts.mileage BETWEEN ' . $data['mileage_from']. ' AND '. $data['mileage_to'];
        }elseif(isset($data['mileage_from'])  and !isset($data['mileage_to'])){
            $conditions[] = 'adverts.mileage > ' . $data['mileage_from'];
        }elseif(!isset($data['mileage_from'])  and isset($data['mileage_to'])){
            $conditions[] = 'adverts.mileage < ' . $data['mileage_to'];
        }

        foreach ($conditions as $key => $condition) {
            if(strlen($where) == 0){
                $where .= '('.$condition.')';
            }else{
                $where .= ' and ('.$condition.')';
            }
        }
        return strlen($where)?$where:'1';
    }

}