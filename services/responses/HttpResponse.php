<?php


namespace Responses;

use Philo\Blade\Blade;

final class HttpResponse implements iResponse
{

    /**
     * Вывод в буфер json строки c соответствующим заголовком
     *
     * @param $json - json строка ответ
     */
    public function json($json)
    {
        header('Content-Type: application/json');
        echo $json;
    }

    /**
     * Вывод в буфер шаблона соответствующей ошибки с кодом ошибки в заголовке
     *
     * @param int $code - код ошибки
     * @return bool
     */
    public function error(int $code)
    {

        http_response_code($code);
        $blade = new Blade(_ROOT_ . '/templates', _ROOT_ . '/cache/blade');
        echo $blade->view()->make('errors/'.$code)->render();

        return false;

    }

    /**
     * Вывод в буфер указанного шаблона
     *
     * @param string $template_name - название blade шаблона
     * @param array $params - входные параметры
     * @return bool
     */
    public function template(string $template_name, array $params)
    {

        $blade = new Blade(_ROOT_ . '/templates', _ROOT_ . '/cache/blade');
        echo $blade->view()->make($template_name, $params)->render();
        return true;
    }

    /**
     * Отправка заголовка перенаправления и прекращение работы программы
     *
     * @param $url - URL страницы, куда необходимо осуществить переход
     */
    public function redirect($url){
        header('Location: http://'.$url);
        exit;
    }
}