<?php

namespace Responses;

interface iResponse
{
    function json(array $param);
    function error(int $code);
    function template( string $template_name, array $params);
}